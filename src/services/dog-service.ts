import type { Dog } from "@/entities";
import axios from "axios";


export async function fetchDogs(){
    const response = await axios.get<Dog[]>('http://localhost:8080/api/dog');
    return response.data;
}

export async function fetchOneDog(id:number|string) {
    const response = await axios.get<Dog>('http://localhost:8080/api/dog/'+id);
    return response.data;
}
export async function postDog(dog:Dog) {
    const response = await axios.post<Dog>('http://localhost:8080/api/dog',dog);
    return response.data;
}

export async function deleteDog(id:number|string) {
    await axios.delete('http://localhost:8080/api/dog/'+id);
}
